package com.simple.andrey.taskmanager;


import java.util.Vector;

public class TasksKeeper {
    private Vector<Task> tasks;

    TasksKeeper() {
        tasks = new Vector<Task>();
    }

    public Task createTask(String name) {

        if (getTask(name) != null) {
            throw new Error("Task exists");
        }

        tasks.addElement(new Task(name));
        return tasks.get(tasks.size());
    }

    public Task getTask(String name) {

        for (Task el: tasks) {

            if (el.getName().equalsIgnoreCase(name)) {
                return el;
            }
        }
        return null;
    }
}
